var express = require('express')
var app = express()
var config_port = 2018;

app.set('port', (process.env.PORT || config_port))
app.use(express.static(__dirname + '/public'))

app.get('/', function(request, response) {
  response.send('Hello World!\n Happy New Year')
})

app.listen(app.get('port'), function() {
  console.log("Node app is running at localhost:" + app.get('port'))
})
