# About project

A our website Node.js app using [Express 4](http://expressjs.com/).

## Running Locally

Make sure you have [Node.js](http://nodejs.org/) installed and the [DigitalOcean Documentation](https://developers.digitalocean.com/documentation/).

```sh
git clone git@github.com:dmfanrus/node-js-sample.git # or clone your own fork
cd node-js-sample
npm install
npm start
```

Your app should now be running on [localhost:32769](http://localhost:32769/).

## Deploying to DigitalOcean

```
something code for deploy process

```

## Documentation

Something documentations
